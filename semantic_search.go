package main

/*
This acts as a wrapper over ./semantic_search.py:

1. Create a virtual environment if it does not exist
2. Embed the semantic_search.py and requirements.txt into the go binary
3. Copy the embedded files to the virtual environment
4. Run the semantic_search.py script using the venv
  - Set notes_dir as first arg or ~/Notes/slipbox
  - Look for an embedding file under the cache
    - This will be relative to the notes directory and model_name
    - In the absense of that file re-embed
  - Ask the user for input
  - Measure the latent distance and sort
  - Return top 10 with some context (works well in vscode or vim with `gf`)
*/

import (
	"embed"
	"fmt"
	"log"
	"os"
	"os/exec"

	"github.com/adrg/xdg"
)

//go:embed semantic_search.py
//go:embed requirements.txt
var content embed.FS

func Sem_Search(notes_dir string) {
	// get the hostname

    var hostname string
	hostname, err := os.Hostname()

	if err != nil {
		fmt.Println("HOSTNAME Unavailable, looking at /etc/hostname")
		fmt.Println(err)

		// Try the /etc/hostname file
        b, err := os.ReadFile("/etc/hostname")
        hostname = string(b)
		if err != nil {
			log.Fatal(err)
		}
	}


	// Set Variables
	// VENV dir
	xdg_cache_home := xdg.CacheHome
	venv_dir := fmt.Sprintf("%s/%s/%s/%s", xdg_cache_home, "go_notetaking", "virtualenvs", hostname)
	py_venv := fmt.Sprintf("%s/bin/python", venv_dir)
	pip_venv := fmt.Sprintf("%s/bin/pip", venv_dir)
	// Requirements and script
	req := fmt.Sprintf("%s/requirements.txt", venv_dir)
	script := fmt.Sprintf("%s/semantic_search.py", venv_dir)

	// Look for the virtual environment and create it if it does not exist
	if _, err := os.Stat(venv_dir); err == nil {
		fmt.Printf("Virtual Environment detected at %s, Using for Semantic Search\n", py_venv)
		fmt.Printf("To remove the virtual environment, run: rm -rf %s\n", venv_dir)
	} else {
		fmt.Println("Virtual Environment Does Not Exist, creating...")

		// Create the virtual environment
		os.MkdirAll(venv_dir, 0755)
		sh("python3", "-m", "venv", venv_dir)
		fmt.Println("Virtual Environment Created")

		// Copy in the semantic_search.py and requirements.txt
		write_external_file(script, "semantic_search.py")
		write_external_file(req, "requirements.txt")

		// Install the requirements
		fmt.Println("Installing Requirements")
		sh(pip_venv, "install", "-r", req)
		fmt.Println("Finished Installing Requirements")
	}

	// Run the semantic search Script
	sh(py_venv, script, notes_dir)
}

func write_external_file(target string, source string) {
	// Read the file from the embedded filesystem
	data, err := content.ReadFile(source)
	if err != nil {
		log.Fatal(err)
	}
	err = os.WriteFile(target, data, 0644)
	if err != nil {
		log.Fatal(err)
	}
}

func sh(cs string, as ...string) {

	// Create a new command
	cmd := exec.Command(cs, as...)

	// Set the standard input and output
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	// Run the command and return the output
	err := cmd.Run()

	// Handle the error
	if err != nil {
		log.Fatal(err)
	}
}
