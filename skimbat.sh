#!/usr/bin/bash
notes_dir="$HOME/Notes"
notes=$(sk -m -i -c  "./note_taking search -d "${notes_dir}"  {}"                         \
    --bind pgup:preview-page-up,pgdn:preview-page-down                \
    --preview "bat --style grid --color=always                        \
                    --terminal-width 80 ${notes_dir}/{+}                           \
                    --italic-text=always                              \
                    --decorations=always"                                    |\
sed "s#^#${notes_dir}/#"
)

echo $notes | xargs nvim

# nvim "${notes}"
