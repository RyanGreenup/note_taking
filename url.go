package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"
)

var FORMATS = map[string][]string{
	"md":  {"markdown", "md"},
	"org": {"org", "o"},
	"mw":  {"mediawiki", "mw", "wiki"},
	"dw":  {"dokuwiki", "dw"},
}

func URL(args []string, notes_dir string, interactiveQ bool) {
	var format string
	var url string
	if len(args) < 1 {
		fmt.Print("Insufficient Arguments:\n\n")
		printHelp(7)
	} else if len(args) == 2 {
		url = args[1]
		format = check_format(os.Args[2])
	}

	// Convert it into a link
	s := parsetitle(url, format)

	// Print it out
	fmt.Print(s)
}

func check_format(f string) string {
	for _, allowed_format := range FORMATS {
		if In(f, allowed_format) {
			return f
		}
	}

	fmt.Print("Format", f, "not recognized\n\n")
	printHelp(1)
	os.Exit(2)
	return f
}

func printHelp(errcode int) {
	fmt.Println("TODO Merge this into the main Help")
	fmt.Println("TODO Swap URL and Format")
	fmt.Println("TODO Implement interactive mode (clipboard)")
	fmt.Println("Take a URL and return a formatted link:")
	fmt.Println(os.Args[0] + " [URL] " + " [FORMAT] ")
	fmt.Print("\nAvailable formats are:\n\n")

	for _, value := range FORMATS {
		fmt.Println(value)
	}

	os.Exit(errcode)
}

func parsetitle(url string, format string) string {

	// Get the HTML from the URL
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println(url)
		log.Fatal(err)
	}
	b, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
	HTML := string(b)

	// Get the description from the HTML
	re_title := regexp.MustCompile(`(?s)(<title>)(.*)(<\/title>)`)
	re_heading := regexp.MustCompile(`(?s)(<h1>)(.*)(<\/h1>)`)

	for _, re := range []*regexp.Regexp{re_title, re_heading} {
		if re.MatchString(HTML) {
			// Find the first match and the second match group
			match_string := re.FindStringSubmatch(HTML)[2]
			desc := match_string
			formatted_link := format_url_description(url, desc, format)
			return formatted_link
		}
	}

	return format_url_description(url, "", format)
}

func format_url_description(url string, desc string, format string) string {

	url_sans_scm := strings.ReplaceAll(url, "http://", "")
	if format == "md" || format == "markdown" {
		if desc != "" {
			return "[" + desc + "]" + "(" + url + ")"
		} else {
			return "<" + url + ">"
		}
	} else if format == "org" {
		if desc != "" {
			return "[[" + url + "]" + "[" + desc + "]]"
		} else {
			return "[[" + url + "]]"
		}
	} else if format == "mediawiki" {
		if desc != "" {
			return "[" + url + " " + desc + "]"
		} else {
			return "[" + url + url_sans_scm + "]"
		}
	} else if format == "dokuwiki" {
		if desc != "" {
			return "[[" + url + "|" + desc + "]]"
		} else {
			return "[[" + url + "|" + url_sans_scm + "]]"
		}
	} else {
		if desc != "" {
			// if nothing org makes the most sense
			return "[[" + url + "]" + "[" + desc + "]]"
		} else {
			return url
		}
	}

}
