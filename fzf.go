package main

import (
	"fmt"
	"log"
	"path/filepath"

	"github.com/ktr0731/go-fuzzyfinder"
)

func Fzf_print(notes_dir string, wikilink bool) {
	if wikilink {
		for _, f := range Fzf(notes_dir) {
			filename_wikilink := "[[" + f + "]]"
			fmt.Println(filename_wikilink)
		}

	} else {
		for _, f := range Fzf(notes_dir) {
			fmt.Println(f)
		}

	}

}

/*
It might seem slow to index everything first, definitely slower than:

```
fzf --preview 'cat {}'
```
but in practice on 700 md files with 100k lines it's instant
*/
func Fzf(notes_dir string) []string {
	// TODO this should be a static array
	files := ListFiles(notes_dir, true)
	results := make([]text_structure, len(files))
	for i, path := range files {

		doc := text_structure{
			Path:    path,
			Base:    filepath.Base(path),
			Content: getFile(path),
		}

		results[i] = doc
	}

	idx, err := fuzzyfinder.FindMulti(
		results,
		func(i int) string {
			return results[i].Path
		},
		fuzzyfinder.WithPreviewWindow(func(i, w, h int) string {
			if i == -1 {
				return ""
			}
			return fmt.Sprintf("%s----------------------------\n\n%s",
				results[i].Path,
				results[i].Content)
		}))
	if err != nil {
		log.Fatal(err)
	}

	matches := []string{}
	for _, id := range idx {
		matches = append(matches, files[id])
	}
	return matches
}
