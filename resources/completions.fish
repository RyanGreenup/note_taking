set go_notes note_taking
complete -c $go_notes -s d -l directory -F
complete -c $go_notes -F -a "-d"         -d "The directory where the notes are sotred"
complete -c $go_notes -f -a "-i"         -d "If used interactive intenal libraries will be used"
complete -c $go_notes -f -a "reindex"    -d "Build an index for the Notes and store it in ./bleve_index"
complete -c $go_notes -f -a "fzf"     -d "Use a built in fzf to match based on file name"
complete -c $go_notes -f -a "search"     -d "Search Notes using the index"
complete -c $go_notes -f -a "searchx"    -d "Search Notes, display via skim and bat (external and must be in PATH)"
complete -c $go_notes -f -a "backlinks"  -d "Return backlinks to the specified note"
complete -c $go_notes -f -a "url"        -d "Lookup and format a URL in markdown"