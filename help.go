package main

import (
	"fmt"
	"os"
)

func Print_help(notes_dir string) {
	width := 80
	commands, flags := Commands()

	// Find the longest length
	var l int
	length := 0
	for _, x := range commands {
		l = len(x[0])
		if length < l {
			length = l
		}
	}

	for _, x := range flags {
		l = len(x[1])
		if length < l {
			length = l
		}
	}

	// Print out the actual content
	fmt.Println("A CLI to search, find and link plain text notes.")

	// Example
	print_heading("\nExample", width)
	fmt.Printf("--> \t%s %s %s %s %s\n",
		fmt.Sprintf(DebugColor, os.Args[0]),
		fmt.Sprintf(InfoColor, "-"+flags[0][0]),
		fmt.Sprintf(InfoColor, notes_dir),
		fmt.Sprintf(WarningColor, commands[3][0]),
		"my search term")
	fmt.Println("")

	padw := length + 17
	pad := "%-" + fmt.Sprintf("%d", padw) + "v"

	// Flags Section
	print_heading("Flags", width)
	for _, flag_details := range flags {
		fmt.Printf("%-2v  "+pad+" %-12v\n",
			fmt.Sprintf(InfoColor, "-"+flag_details[0]),
			fmt.Sprintf(InfoColor, "--"+flag_details[1]),
			flag_details[2])
	}

	// Commands Section
	padw = padw + 4 // '-d --' adds 4 characters (not 5 which is odd)
	pad = "%-" + fmt.Sprintf("%d", padw) + "v"
	print_heading("\nCommands", width)
	for _, command_details := range commands {
		fmt.Printf(pad+" %-12v\n",
			fmt.Sprintf(InfoColor, command_details[0]),
			command_details[1])
	}

}

func print_heading(s string, width int) {
	width = width - len(s)
	fmt.Printf(NoticeColor, s)
	for i := 0; i < width; i++ {
		fmt.Print("_")
	}
	fmt.Println("")

}
