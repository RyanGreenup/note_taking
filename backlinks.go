package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

const SEP_MARKER string = `* *__Backlinks__*`

// https://gist.github.com/ik5/d8ecde700972d4378d87

func Write_BLS(notes_dir string, remove bool, mdlink bool) {

	err := filepath.Walk(notes_dir,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			// NOTE https://gobyexample.com/regular-expressions
			if In(filepath.Ext(path), []string{".org", ".md", ".txt"}) {
				// TODO review this approach, may be looping through files twice
				// needlessly
				// Remove the old backlinks
				remove_backlinks(path)

                // insert backlinks
                if remove==false{

				    // get the backlinks
				    matches := backlinks(path, notes_dir)

				    // Write the backlinks
				    insert_backlinks(path, matches, mdlink)
                }
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}
}

/*
Expects a file with a text marker at the end (`begin_block`)
then

the backlinks will be placed inside any occurence of 13 dashes.
*/
func insert_backlinks(path string, bs []string, mdlink bool) {

	contentsb, err := ioutil.ReadFile(path)
	contents := string(contentsb)
	if err != nil {
		log.Fatal(err)
	}

	// Don't add backlinks to sidebar files
	if strings.Contains(path, "_Sidebar") {
		return
	}

	if strings.Contains(contents, SEP_MARKER) {
		// NOTE this throws away bottom half
		// TODO Using strings.index would allow this to cut out
		//  anything between a begin and end block (e.g. #+begin_bl / #+end_bl)
		// TODO MUST first remove backinks, with a seperate function
		results := strings.Split(contents, SEP_MARKER)
		output := ""
		output += fmt.Sprintln(results[0])
		output += fmt.Sprintln(SEP_MARKER)
		for i := range bs {
			// NOTE make this relative path to support non-flat dir structures
			relpath, err := filepath.Rel(filepath.Dir(path), bs[i])
			if err != nil {
				relpath = filepath.Base(bs[i])
				log.Println(err)
			}
			// output += fmt.Sprintln("  - [[" + relpath + "]]")
            // TODO make this an option

            if mdlink==true{
                // print the backlink as a markdown link
                // replace spaces in relpath with %20
                relpath = strings.ReplaceAll(relpath, " ", "%20")
                displaytext := filepath.Base(bs[i])
                // remove extension
                displaytext = Strip_extension(displaytext, true)
                // replace - with space in display text
                displaytext = strings.ReplaceAll(displaytext, "-", " ")
                // capitalise the first letter of each word
                displaytext = strings.Title(displaytext)

                output += fmt.Sprintln("  - [" + displaytext + "](" + relpath + ")")
            } else {
                //wiki link
			    output += fmt.Sprintln("  - [[" + relpath + "]]")
            }
		}
		os.WriteFile(path, []byte(output), 0644)
	} else {
		// Add the backlinks section and try again
		output := contents
		// Add the marker
		output += fmt.Sprintln(SEP_MARKER)
		// Write the file
		os.WriteFile(path, []byte(output), 0644)
		// Call this function again
		insert_backlinks(path, bs, mdlink)
	}

}

func remove_backlinks(path string) {

	contentsb, err := ioutil.ReadFile(path)
	contents := string(contentsb)
	if err != nil {
		log.Fatal(err)
	}

	// Don't add backlinks to sidebar files
	if strings.Contains(path, "_Sidebar") {
		return
	}

	if strings.Contains(contents, SEP_MARKER) {
		// NOTE this throws away bottom half
		// TODO Using strings.index would allow this to cut out
		//  anything between a begin and end block (e.g. #+begin_bl / #+end_bl)
		results := strings.Split(contents, SEP_MARKER)
		output := ""
		output += fmt.Sprintln(results[0])
		os.WriteFile(path, []byte(output), 0644)
	}
}

/*
BLS is a function that takes a:

  - a slice of filenames
  - a top directory

and prints all backlinks to stdout
*/
func BLS(args []string, notes_dir string, interactiveQ bool) {
	var filenames []string

	// Get the filename to search for
	if interactiveQ {
		// Use fzf to do this so it's more interactive
		filenames = Fzf(notes_dir)
	} else {
		// Take it from the command line
		if len(args) < 1 {
			log.Fatal("First argument should be target note file")
		}
		filenames = args
	}

	// Look for a backlink to the file path and append it to a larger list
	all_matches := []string{}
	var matches []string

	for _, filename := range filenames {
		matches = backlinks(filename, notes_dir)
		all_matches = append(all_matches, matches...)
	}

	// Print out the matches
	for _, file := range all_matches {
		fmt.Println(file)
	}
}

func backlinks(filename string, top_dir string) []string {
	filename = filepath.Base(filename)
	filename_noext := Strip_extension(filename, true)
	// filename_wikilink := "[[" + filename_noext + "]]"
    filename_wikilink := fmt.Sprintln("  - [" + filename + "](" + filename_noext + ")")

	matches := grep(filename, top_dir)
	matches = append(matches, grep(filename_wikilink, top_dir)...)
	matches = unique(matches)

	return matches
}

/*
This removes duplicate entries from a string array

See https://www.golangprograms.com/remove-duplicate-values-from-slice.html
*/
func unique(str_slice []string) []string {
	keys := make(map[string]bool)
	list := []string{}
	for _, entry := range str_slice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func Strip_extension(filename string, base bool) string {
	if base {
		filename = filepath.Base(filename)
	}
	ext := filepath.Ext(filename)
	// Reverse to match extension at end
	// https://stackoverflow.com/a/27945044
	filename_noext := Rev(strings.Replace(Rev(filename), Rev(ext), "", 1))
	return filename_noext
}

func Rev(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

func loop_through_contents(path string, search_term string) bool {
	file, err := os.Open(path)
	if err != nil {
		// log.Println(err)
		return false
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, search_term) {
			return true
		}
	}

	if err := scanner.Err(); err != nil {
		l := log.New(os.Stderr, "", 0)
		l.Println("--------------------------------------------------------------------------------")
		l.Println("\nError Reading:\n", path, "\n", err)
		l.Println("--------------------------------------------------------------------------------")
		l.Println(" \n|\n ")
	}

	// If no matches were found just return false
	return false
}

func grep(search_term string, top_dir string) []string {

	matches := []string{}
	top_dir, err := filepath.EvalSymlinks(top_dir)
	if err != nil {
		fmt.Println("Unable to follow symlink of\n", top_dir)
	}

	err = filepath.Walk(top_dir,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			// NOTE https://gobyexample.com/regular-expressions
			if In(filepath.Ext(path), []string{".org", ".md", ".txt"}) {
				if loop_through_contents(path, search_term) {
					matches = append(matches, path)
				}
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}

	return matches

}
