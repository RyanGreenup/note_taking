package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/ktr0731/go-fuzzyfinder"
)

func In(s string, ss []string) bool {
	for key, val := range ss {
		_ = key
		if s == val {
			return true
		}
	}

	return false
}

func clean(args []string) {
	if len(args) < 1 {
		help()
		return
	}
	dir := args[0]
	args = args[1:]
	os.Chdir(dir)
	rename()
}

func rename() {
	current_dir, err := os.Getwd()
	if err != nil {
		log.Fatal("Unable to resolve directory provieded")
	}
	fmt.Printf("cd " + `'` + current_dir + `'` + "\n")

	err = filepath.Walk(".",
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if filepath.Ext(path) == ".md" || filepath.Ext(path) == ".org" {
				// fmt.Println()
				extract_title(path)
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}

}

func extract_title(file_path string) {
	file, err := os.Open(file_path)
	if err != nil {
		log.Println("Unable to open the file saldjf")
		log.Println(err)
		return
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	// optionally resize scanner's capacity for lines over 64k
	count := 0
	for scanner.Scan() && count < 3 {
		line := scanner.Text()
		matchq, title := is_title(line, file_path)
		if matchq {
			ext := filepath.Ext(file_path)
			d := filepath.Dir(file_path)
			fmt.Printf("mv %-40s %s", `'`+file_path+`'`, `'`+d+"/"+title+ext+`'`+"\n")
			break
		}
		count++
	}

	if err := scanner.Err(); err != nil {
		log.Println("Unable to open the file lkjdsao")
		log.Println(err)
	}

}

func is_title(line string, filename string) (bool, string) {
	// yaml_regex := `^title: (.+)`
	// org_title_regex := `TITLE`
	var regexes []string
	ext := filepath.Ext(filename)
	if ext == ".md" || ext == ".org" {
		regexes = []string{
			`(?i)` + `^#\+title:\s*(.+)`, // Org Mode
			// `^#\s(.+)`, // First Heading
			// `^title: (.+)`,               // yaml
		}
	} else if ext == ".md" {
		regexes = []string{
			`^title: (.+)`, // yaml
			`^#\s(.+)`,     // First Heading
		}
	} else {
		return false, ""
	}

	matchq := false
	for _, rs := range regexes {
		re := regexp.MustCompile(rs)
		if re.MatchString(line) {
			title := re.FindStringSubmatch(line)[1]
			return true, title
		}

	}

	return matchq, ""

}

func help() {
	fmt.Println("Usage:")
	fmt.Println("\t First argument should be the directory containing the files to clear")
}

// Function to make a new note
func New(args []string, notes_dir string) {

	// Interpret the arguments fed in to get the filename
	var name string
	var err error
	if len(args) != 0 {
		for i := range args {
			name += " " + args[i]
		}
	} else {
		fmt.Println("No Filename argument detected")
		fmt.Println("Choose a namespace or skip with C-c.")
		Enter_to_continue()

		namespace := fzf_namespace(notes_dir)

		fmt.Println("\n", namespace, "\n")
		reader := bufio.NewReader(os.Stdin)
		fmt.Printf("Type a filename and Press enter to continue\n\n\t-->")
		name, err = reader.ReadString('\n')
		if err != nil {
			log.Fatal(err.Error())
		}
		name = strings.ReplaceAll(name, "\n", "")
		name = namespace + "/" + name
	}
	fmt.Println(name)

	// What strings following dots will we recognize as .exts?
	extensions := []string{"org", "md", "markdown", "tex", "txt", "mw", "mediawiki"}

	// Rename accordingly with the `.` and `/` stuff logseq does
	var title string
	var filename string
	ext := filepath.Ext(name)

	if In(ext, extensions) {
		// Reverse to match extension at end
		// https://stackoverflow.com/a/27945044
		name = Rev(strings.Replace(Rev(name), Rev(ext), "", 1))
		title = strings.ReplaceAll(name, ".", "/")
		filename = strings.ReplaceAll(name, "/", ".")
		filename = filename + ext
	} else {
		title = strings.ReplaceAll(name, ".", "/")
		filename = strings.ReplaceAll(name, "/", ".")
		filename = filename + ".org"

	}
	title = "#+TITLE: " + title

	// Now create the file
	notes_dir = notes_dir  // NOTE this assumes a flat notes directory, no pages/ dir like logseq
	// Normalize the directory and check it exist
	notes_dir, err = filepath.Abs(notes_dir)
	if err != nil {
		fmt.Println("Unable to find location of ", notes_dir)
		log.Fatal(err.Error())
	}
	err = os.Chdir(notes_dir)
	if err != nil {
		fmt.Println("Unable to change directory into :\n", notes_dir)
		log.Fatal(err.Error())
	}
	// either append or create the file
	err = os.WriteFile(filename, []byte(title), 0644)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("---> ", title)
	fmt.Println("--->          ", filename)

}

func fzf_namespace(notes_dir string) string {

	// Get the category names
	matches := []string{}

	err := filepath.Walk(notes_dir,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			// NOTE https://gobyexample.com/regular-expressions
			if In(filepath.Ext(path), []string{".org", ".md", ".txt"}) {
                // Also get basename
				path = Strip_extension(path, true)
				path = Strip_extension(path, true)
				if strings.Contains(path, ".") {
					matches = append(matches, path)
				}
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}

	// Remove non-unique category names
	unique := []string{}
	for _, namespace := range matches {
		if !In(namespace, unique) {
			unique = append(unique, namespace)
		}
	}

	// Uze fuzzyfinder to choose one
	idx, err := fuzzyfinder.Find(unique, func(i int) string { return unique[i] })
	if err != nil {
		// fmt.Println("No output from fuzzyfinder")
		 log.Fatal(err.Error())
	}
	return unique[idx]

}

func Enter_to_continue() {
	fmt.Println("Press Enter to Continue.")
	var somestringstuff string
	fmt.Scanf("%s", &somestringstuff)
	_ = somestringstuff
}
