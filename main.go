package main

import (
	"os"
	"path/filepath"

	flag "github.com/spf13/pflag"
)

const (
	InfoColor    = "\033[1;34m%s\033[0m"
	NoticeColor  = "\033[1;36m%s\033[0m"
	WarningColor = "\033[1;33m%s\033[0m"
	ErrorColor   = "\033[1;31m%s\033[0m"
	DebugColor   = "\033[0;36m%s\033[0m"
	OptColor     = "\033[0;36m%v\033[0m"
)

func main() {
	commands, flags := Commands()
	_ = commands
	default_notes_dir := filepath.Clean(os.Getenv("HOME") + "/Notes/slipbox")

	notes_dir_f := flag.StringP(flags[0][1], flags[0][0], default_notes_dir, flags[0][2])
	interactiveQ := flag.BoolP(flags[1][1], flags[1][0], false, flags[1][2])
	helpQ := flag.BoolP(flags[2][1], flags[2][0], false, flags[2][2])

	flag.Parse()

	notes_dir := filepath.Clean(*notes_dir_f)

	if *helpQ {
		Print_help(notes_dir)
		os.Exit(0)
	}

	/* Change to Notes Directory so paths can be relative,
	deep structures directory structures would be hard to read in the terminal.  */
	os.Chdir(notes_dir)
	parse_subcommands(notes_dir, *interactiveQ, flag.Args())

}

func Commands() ([][]string, [][]string) {
	commands := [][]string{
		{"help", "Display the help"},
		{"fzf", "Use a built in fzf to match based on file name"},
		{"search", "Search using built in libraries/tools, terms should be first arguments"},
		{"searchx", "Search interactively using skim and bat (=sk=, =bat =and =sh= must be in PATH)."},
		{"reindex", "Reindex Search results."},
		{"backlinks", "Print backlinks to given note."},
		{"write-bl", "Print backlinks to given note."},
		{"url", "Return the URL as a formatted markdown link."},
		{"link", "Print a [[Wikilink]] to the standard output"},
		{"rename", "Rename a directory of files based on the =#TITLE:=, yaml title or =# heading= respectively"},
		{"new", "Create a new note"},

		{"remove-bl", "Removes backlinks to a given note."},
		{"write-bl-wiki", "Inserts wikilinks styled backlinks to a given note."},

		{"sem", "Use Python and Langchain to Perform a Semantic Search"},
		{"tantivy", "Use Tantivy to perform a Search"},
		{"tantivy-index", "Re-index the Tantivy Search"},
	}

	flags := [][]string{
		// TODO this help should look for default_notes_dir variable
		{"d", "directory", "Specify the directory containing notes (defaults to ~/Notes/slipbox)"},
		{"i", "interactive", "If specified, user will be prompted for input"},
		{"h", "help", "Print out the Help"},
	}
	return commands, flags
}

func parse_subcommands(notes_dir string, interactiveQ bool, args []string) {
	if len(args) < 1 {
		Print_help(notes_dir)
		return
	}
	// Pop the subcommand off
	value := args[0]
	args = args[1:]

	commands, _ := Commands()

	switch value {
	default:
		Print_help(notes_dir)
	// help
	case commands[0][0]:
		Print_help(notes_dir)
	// fzf
	case commands[1][0]:
		Fzf_print(notes_dir, false)
	// Search
	case commands[2][0]:
		Search(args, interactiveQ, notes_dir)
	// SearchX with bat/skim
	case commands[3][0]:
		Skimbat(notes_dir)
	// Reindx
	case commands[4][0]:
		Reindex(args, notes_dir)
	// Backlinks
	case commands[5][0]:
		BLS(args, notes_dir, interactiveQ)
		// Write Backlinks
	case commands[6][0]:
		Write_BLS(notes_dir, false, true)
	// URL
	case commands[7][0]:
		URL(args, notes_dir, interactiveQ)
		// Forward link
	case commands[8][0]:
		Fzf_print(notes_dir, false)
		// Clean
	case commands[9][0]:
		clean(args)
	case commands[10][0]:
		New(args, notes_dir)

	// Remove Backlinks
	case commands[11][0]:
		// Remove_BLS(notes_dir)
		Write_BLS(notes_dir, true, true)

	// Write Wikilink Backlinks
	// TODO order these to be nicer
	case commands[12][0]:
		Write_BLS(notes_dir, false, false)

    // Sematntic Search
	case commands[13][0]:
		Sem_Search(notes_dir)

    // Tantivy Search
	case commands[14][0]:
		Tantivy(notes_dir)

    // Tantivy Reindex
	case commands[15][0]:
		TantivyReindex(notes_dir)

	}

}
