package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/adrg/xdg"
	"github.com/blevesearch/bleve/v2"
	"github.com/ktr0731/go-fuzzyfinder"
	"github.com/schollz/progressbar/v3"
)

type text_structure struct {
	Path    string
	Base    string
	Content string
}

func Search(args []string, interactiveQ bool, notes_dir string) {
	var search_term string

	// query := bleve.NewMatchQuery(search_term)
	// search := bleve.NewSearchRequest(query)

	// TODO implement XDG lookup
	// convert notes_dir to a single word with underscore, no spaces and --
	index_path := get_index_path(notes_dir)

	index, err := bleve.Open(index_path)
	if err != nil {
		log.Fatal(err)
	}

	if interactiveQ {
		search_term = getInput()
	} else {
		search_term = combineInputSlice(args)
	}

	search_results := do_search(index, search_term, 10)

	if interactiveQ {
		fzf_interactive_search(search_results)
	} else {
		for i := 0; i < len(search_results); i++ {
			fmt.Println(search_results[i])
		}
	}

}

func Skimbat(notes_dir string) {
	cmd :=
		`sk -i -c "%s search -d %s {}"                         \
    --bind pgup:preview-page-up,pgdn:preview-page-down                \
    --preview "bat --style grid --color=always                        \
                    --terminal-width 80 {+}                           \
                    --italic-text=always                              \
                    --decorations=always" | sed 's#^#%s/#'`
	cmd = fmt.Sprintf(cmd, os.Args[0], notes_dir, notes_dir)

	out, err := exec.Command("sh", "-c", cmd).Output()
	if err != nil {
		log.Printf("Failed to execute command: %s", cmd)
		log.Println(err)
	}
	fmt.Print(string(out))
}
func get_index_path(notes_dir string) string {
    // TODO this name could be improved
	notes_dir_name := strings.Replace(notes_dir, xdg.Home, "", 1)
	// notes_dir_name := strings.Replace(notes_dir, "/", "", 1)
	notes_dir_name = strings.Replace(notes_dir, "/", "--", -1)
	notes_dir_name = strings.Replace(notes_dir_name, " ", "_", -1)

	index_path := fmt.Sprintf("%s/note_taking_go/%s/bleve.index", xdg.CacheHome, notes_dir_name)
	return index_path
}
func Reindex(args []string, notes_dir string) {

	index_path := get_index_path(notes_dir)

	files := ListFiles(notes_dir, true)

	_ = make_index(index_path, files)

}

func make_index(index_path string, files []string) bleve.Index {

	// TODO This should have more error checking, what if there is just a write permission?
	// what if it's the wrong file? then it would be overwritten...
	index, err := bleve.Open(index_path)
	if err != nil {
		log.Println(err)
		log.Println("Creating a New index")
		mapping := bleve.NewIndexMapping()
		index, err = bleve.New(index_path, mapping)
		if err != nil {
			fmt.Print(err)
			fmt.Print("Unable to Create new index")
			os.Exit(1)
		}
	} else {
		fmt.Println("Appending to Old Index")
	}

	bar := progressbar.Default(int64(len(files)))
	i := 1

	for _, file := range files {
		// fmt.Println(file)
		i = i + 1

		notecontent := getFile(file)

		data3 := text_structure{
			Path:    file,
			Content: notecontent,
		}

		// documents = append(documents, data3)
		// fmt.Println(file)

		index.Index(data3.Path, data3)

		bar.Add(1)

	}

	fmt.Println("Successfully Created index at", index_path)

	return index
}

func getFile(path string) string {
	buf, err := os.ReadFile(path)
	notecontent := string(buf)

	if err != nil {
		fmt.Printf("Error Reading File:\n\t%s\n", path)
	}
	return notecontent
}

func ListFiles(top_dir string, relative_to_homeQ bool) []string {
	files := []string{}

	echo := func(path string, info os.FileInfo, err error) error {

		if info.IsDir() {
			return nil
		}

		ext := filepath.Ext(path)
		if !(ext == ".md" || ext == ".txt" || ext == ".org" || ext == ".tex" || ext == ".markdown") {
			return nil
		}

		if relative_to_homeQ {
			path, err = filepath.Rel(top_dir, path)
			if err != nil {
				log.Println("Unable to get relative path from:\n", top_dir, " to ", path)
			}
		}

		files = append(files, path)

		// fmt.Print(path)
		return nil

	}

	top_dir, err := filepath.EvalSymlinks(top_dir)
	if err != nil {
		log.Println("Unable to follow symlink of\n", top_dir)
	}
	err = filepath.Walk(top_dir, echo)
	if err != nil {
		panic(err)
	}

	return files
}
func getInput() string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter a Search Term:\n")
	term, _ := reader.ReadString('\n')
	return term

}

func fzf_interactive_search(search_resuts []string) {
	// TODO this should be a static array
	results := make([]text_structure, len(search_resuts))
	for i, path := range search_resuts {

		doc := text_structure{
			Path:    path,
			Base:    filepath.Base(path),
			Content: getFile(path),
		}

		results[i] = doc
	}

	idx, err := fuzzyfinder.FindMulti(
		results,
		func(i int) string {
			return results[i].Base
		},
		fuzzyfinder.WithPreviewWindow(func(i, w, h int) string {
			if i == -1 {
				return ""
			}
			return fmt.Sprintf("%s----------------------------\n\n%s",
				results[i].Base,
				results[i].Content)
		}))
	if err != nil {
		log.Fatal(err)
	}
	for _, id := range idx {
		fmt.Println(search_resuts[id])
	}
}

func combineInputSlice(args []string) string {
	term := ""
	for _, word := range args {
		term = term + " " + word
	}

	return term
}

func do_search(index bleve.Index, search_term string, n_matches int) []string {

	query := bleve.NewMatchQuery(search_term)
	search := bleve.NewSearchRequest(query)

	// Max the number of results if we set the upper limit to 0
	if n_matches == 0 {
		n_matches_uint, err := index.DocCount()
		n_matches = int(n_matches_uint)
		if err != nil {
			log.Panic(err)
		}
	}
	search.Size = int(n_matches)

	searchResults, err := index.Search(search)

	if err != nil {
		fmt.Println("No Search Results :( ")
		fmt.Println(err)
		return []string{}
	}

	// Print out the Results
	// TODO Only print out the File Path
	// TODO, give this file path to fuzzy-finder-go --preview
	// fmt.Println(searchResults) // This prints everything
	var search_results []string
	for j := 0; j < searchResults.Hits.Len(); j++ {
		search_results = append(search_results, searchResults.Hits[j].ID)
		// fmt.Println() // This prints the ID of the first
	}

	return search_results

}
